import Cities from "./cities";

const routes = (server) => {


  server.get("/cities", Cities.getNeighbouringCities);
  server.get("/cities/:city_id", Cities.getCityByCityId);
  server.get("/cities/:city_id/weather", Cities.getWeatherByCityId);

};

export default routes;
