import restify from "restify";
import {citiesList} from "../../Constant";

const getCityByCityId = (req,res,next)=>{
	const city_id = req.params.city_id || -1;
	if (city_id <= -1){
		 res.send(400,new restify.BadRequestError("Bad request : CityId is not valid"));
		 return next();
	}
	if(!(city_id in citiesList.json_file)){
		 res.send(404,new restify.NotFoundError("Not found"));
		 return next();
	}
	let cityJson = citiesList.json_file[city_id];
	let apiresponse = {};

	apiresponse.id = cityJson.id;
	apiresponse.name = cityJson.name;
	apiresponse.lat = cityJson.coord.lat;
	apiresponse.lng = cityJson.coord.lon;
	res.json(apiresponse);
	return next();
};
export default getCityByCityId;
