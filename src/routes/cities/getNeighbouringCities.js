import request from "request";
import moment from "moment";
import restify from "restify";
import {url} from "../../Constant";
import citiesJson from "../../city.list"
import {getDistanceFromLatLonInKm} from "../../util/utils";


const getNeighbouringCities = (req,res,next)=>{

		if(req.params.lat == undefined || req.params.lng == undefined){
			res.send(400,new restify.BadRequestError("Lat/lng required"))
			return next();
		}
		const lat = req.params.lat;
		const lng = req.params.lng;
		if(lat > 90 || lat < -90 || lng > 180 || lng < -180){
			res.send(400,new restify.BadRequestError("Invalid lat/lng"));
			return next();
		}
		let apiresponse = [];
		citiesJson.forEach((value,key)=>{
			if(getDistanceFromLatLonInKm(lat,lng,value.coord.lat,value.coord.lon) <= 10){
					let cityObject = {id :value.id, name :value.name };
					apiresponse.push(cityObject);
			}
		});
		res.json(apiresponse);
		return next();
};
export default getNeighbouringCities;
