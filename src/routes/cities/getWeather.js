import request from "request";
import moment from "moment";
import restify from "restify";
import {url} from "../../Constant";

const getWeatherByCityId= (req,res,next)=>{
	const city_id = req.params.city_id || -1;
	if (city_id == -1){
		 res.send(400,new restify.BadRequestError("Bad request"));
		 return next();
	}
	request.get( url + city_id,(request,response,body)=>{
		const responseBody = JSON.parse(body);
		let apiresponse = {};

		if (!(/^2/.test('' + response.statusCode))){
			res.send(404,new restify.NotFoundError("Not Found"));
			return next();
		}
		apiresponse.type = responseBody.weather[0].main;
		apiresponse.type_descritpion = responseBody.weather[0].description;
		apiresponse.sunrise = moment(responseBody.sys.sunrise*1000).format("YYYY-MM-DD[T]H:mm:ss[.000Z]");
		apiresponse.sunset = moment(responseBody.sys.sunset*1000).format("YYYY-MM-DD[T]H:mm:ss[.000Z]");
		apiresponse.humidity = responseBody.main.humidity;
		apiresponse.pressure = responseBody.main.pressure;
		apiresponse.clouds_percent = responseBody.main.clouds_all;
		apiresponse.temp_min = responseBody.main.temp ;
		apiresponse.temp_max = responseBody.main.temp_max ;
		apiresponse.temp = responseBody.main.temp ;
		apiresponse.humidity = responseBody.main.humidity;
		apiresponse.wind_speed = responseBody.wind.speed;
		res.json(apiresponse);

	});

};
export default getWeatherByCityId;
