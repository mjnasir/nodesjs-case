import getNeighbouringCities from "./getNeighbouringCities";
import getCityByCityId from "./getCity";
import getWeatherByCityId from "./getWeather";

module.exports = {
  getWeatherByCityId,
  getCityByCityId,
  getNeighbouringCities
};
